const intervalo = function (b) {
    if (0 < b && b <= 25.00) {
        console.log(`O número está entre o intrvalo [0, 25]`)
    } else if (25.00 <= b && b <= 50.00) {
        console.log(`O número está entre o intrvalo (25, 50]`)
    } else if (50.00 < b && b <= 75.00) {
        console.log(`O número está entre o intrvalo (50, 75]`)
    } else if (75.00 < b && b <= 100.00) {
        console.log(`O número está entre o intrvalo (75, 100]`)
    } else {
        console.log(`Fora de intervalo`)
    }
}

intervalo(17.2)